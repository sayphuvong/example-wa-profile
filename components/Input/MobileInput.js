import { withStyles } from '@material-ui/core';
import React from 'react'
import PropTypes from 'prop-types'

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    color: theme.palette.text.checkbox + '36',
    marginBottom: '0.875rem',

    '& input': {
      width: '100%',
      color: theme.palette.text.checkbox,
      backgroundColor: theme.palette.text.checkbox + '24',
      paddingRight: '4.5rem',
    }
  },
  inputWrap: {
    position: 'relative',
  },
  verifyBtn: {
    position: 'absolute',
    top: '50%',
    right: '1rem',
    textDecoration: 'underline',
    transform: 'translateY(-50%)',
    color: theme.palette.text.checkbox,
    cursor: 'pointer',
  },
});

function MobileInput(props) {
  const {
    classes,
    phone,
    onChange,
    onVerify,
    label,
  } = props;

  const verifyOnClick = () => {
    if (onVerify) {
      onVerify();
    }
  };

  const handlePhoneChange = (e) => {
    if (onChange) {
      const value = e.target.value;
      onChange(value);
    }
  };
  
  return (
    <div className={classes.root}>
      <label>{label}</label>
      <div className={classes.inputWrap}>
        <input type="text" value={'**********' + phone.slice(-5)} onChange={handlePhoneChange} />
        <div className={classes.verifyBtn} onClick={verifyOnClick}>Verify</div>
      </div>
    </div>
  )
}

MobileInput.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  phone: PropTypes.string,
  onChange: PropTypes.func,
  onVerify: PropTypes.func,
};

MobileInput.defaultProps = {
  classes: {},
  label: '',
  phone: '',
  onChange: null,
  onVerify: null,
};

export default withStyles(styles)(MobileInput);