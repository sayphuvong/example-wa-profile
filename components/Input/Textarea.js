import { withStyles } from '@material-ui/core';
import React from 'react'
import PropTypes from 'prop-types'

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    color: theme.palette.text.checkbox + '36',
    marginBottom: '0.875rem',
  },
  textarea: {
    color: theme.palette.text.checkbox,
    backgroundColor: theme.palette.text.checkbox + '24',
  }
});

function Textarea(props) {
  const {
    classes,
    value,
    onChange,
    label,
  } = props;

  const handleTextareaChange = (e) => {
    if (onChange) {
      const value = e.target.value;
      onChange(value);
    }
  };

  return (
    <div className={classes.root}>
      <label>{label}</label>
      <textarea
        className={classes.textarea}
        value={value}
        onChange={handleTextareaChange}
        rows="4"
      />
    </div>
  )
}

Textarea.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onVerify: PropTypes.func,
};

Textarea.defaultProps = {
  classes: {},
  label: '',
  value: '',
  onChange: null,
  onVerify: null,
};

export default withStyles(styles)(Textarea);