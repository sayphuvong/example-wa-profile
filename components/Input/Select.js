import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types'

import ArrowDown from '../SVG/arrow-down';
import colors from '../../src/configs/colors';


const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    color: theme.palette.text.checkbox + '36',
    marginBottom: '0.875rem',
  },
  inputWrap: {
    position: 'relative',
  },
  arrowDown: {
    width: '1rem',
    height: '1rem',

    position: 'absolute',
    top: '50%',
    right: '1rem',

    textDecoration: 'underline',
    transform: 'translateY(-50%)',
  },
  langSelect: {
    width: '100%',
    color: theme.palette.text.checkbox,
    backgroundColor: theme.palette.text.checkbox + '24',
    paddingRight: '2.5rem',
  },
});

function Select(props) {
  const {
    classes,
    label,
    optionData,
    value,
    onChange,
  } = props;

  const handleSelectChange = (e) => {
    if (onChange) {
      const value = e.target.value;
      onChange(value);
    }
  }

  return (
    <div className={classes.root}>
      <label>{label}</label>
      <div className={classes.inputWrap}>
        <select
          className={classes.langSelect}
          value={value}
          onChange={handleSelectChange}
        >
          {optionData && optionData.map((opItem, opIndex) => {
            return (
              <option key={`select-${opItem}-${opIndex}`}>{opItem}</option>
            );
          })}
        </select>
        <div className={classes.arrowDown}>
          <ArrowDown isDepend={true} bgColor={colors.textColor} />
        </div>
      </div>
    </div>
  )
}

Select.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  optionData: PropTypes.array,
  value: PropTypes.string,
  onChange: PropTypes.func,
}

Select.defaultProps = {
  classes: {},
  label: '',
  optionData: [],
  value: '',
  onChange: null,
}


export default withStyles(styles)(Select)