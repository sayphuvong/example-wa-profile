import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

const styles = (theme) => ({
  root: {
    color: props => props.theme === 'dark' ? theme.palette.text.title : theme.palette.text.secondary,
    backgroundColor: props => props.theme === 'dark' ? theme.palette.background.default : 'transparent',
    padding: props => props.theme === 'dark' ? theme.spacing(3) : 0,
    borderRadius: '5px',
    fontSize: '18px',
  },
  title: {
    margin: 0,
    marginBottom: theme.spacing(1),
    fontWeight: 700,
  }
});

function FromGroup(props) {
  const {
    classes,
    children,
    title,
    theme,
  } = props;
  return (
    <div className={classes.root}>
      <p className={classes.title}>{title}</p>
      {children}
    </div>
  )
}

FromGroup.propTypes = {
  classes: PropTypes.object,
  children: PropTypes.element,
  title: PropTypes.string,
  theme: PropTypes.string,
};

FromGroup.defaultProps = {
  classes: {},
  children: null,
  title: '',
  theme: 'light',
};

export default withStyles(styles)(FromGroup)