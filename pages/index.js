import { useState, useEffect } from 'react';

import Tab from '../components/Tab';
import UserIcon from '../components/SVG/user-icon';
import BankIcon from '../components/SVG/bank-icon'
import EmailIcon from '../components/SVG/email-icon'
import colors from '../src/configs/colors'
import AccountProfile from '../components/AccountProfile';

import styles from '../styles/Home.module.scss'

const personalData = {
  id: '100000',
  avatar: '/img/avatar.png',
  name: 'Howdycandidate',
  money: {
    data: '565.000',
    currency: 'THB',
  },
  phone: '(+84)9176566467',
  townCity: 'New York',
  postalCode: '00000',
  address: '132, My Street, Kingston, New York',
  userName: 'howdycandidate',
  password: '$longumanino#$dumpmysolaha',
  dateOfBirth: '12/10/1999',
  country: 'Chile',
  lastName: 'pewpew',
  email: 'email@example.com',
  gender: 'male',
};

function EmptyComp() {
  return (
    <div style={{ width: '100%' }}></div>
  );
}

export default function Home() {
  const [tabList, _] = useState(() => {
    return [
      {
        icon: <UserIcon bgColor={colors.textMainColor} />,
        label: "Account Profile",
        component: <AccountProfile personalData={personalData}/>
      },
      {
        icon: <BankIcon bgColor={colors.textMainColor} />,
        label: "Update Bank Detail",
        component: <EmptyComp />
      },
      {
        icon: <EmailIcon bgColor={colors.textMainColor} />,
        label: "Inbox",
        component: <EmptyComp />
      },
    ];
  });

  return (
    <div className={styles.container}>
      <Tab tabList={tabList} />
    </div>
  )
}
