import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import PropTypes from 'prop-types'

import TabPanel from './TabPanel'

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.secondary,
    margin: `0 auto`,
    minWidth: '100%',
    maxWidth: '1440px',
    
    [theme.breakpoints.up('md')]: {
      minWidth: '80%',
    }
  },
  tabs: {
    backgroundColor: 'red',

    '& .MuiTabs-flexContainer': {
      flexDirection: 'column',
    },

    '& .MuiTab-wrapper': {
      justifyContent: 'left',
    },

    [theme.breakpoints.up('md')]: {
      '& .MuiTabs-flexContainer': {
        flexDirection: 'row',
      },

      '& .MuiTab-wrapper': {
        justifyContent: 'center',
      }
    }
  }
})

function SimpleTabs({ classes, tabList }) {
  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" elevation={0}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label=""
          className={classes.tabs}
        >
          {tabList && tabList.map((tabItem, tabIndex) => {
            return (
              <Tab
                key={`personal-tab-${tabIndex}`}
                icon={(
                  <span>
                    {tabItem.icon}
                  </span>
                )}
                label={tabItem.label}
                {...a11yProps(tabIndex)}
              />
            );
          })}
        </Tabs>
      </AppBar>

      {tabList && tabList.map((tabItem, tabIndex) => {
        const TabContentComp = tabItem.component;
        return TabContentComp && (
          <TabPanel key={`tabPanel-${tabIndex}`} value={value} index={tabIndex}>
            {TabContentComp}
          </TabPanel>
        );
      })}
    </div>
  )
}

SimpleTabs.propTypes = {
  tabList: PropTypes.array,
};

SimpleTabs.defaultProps = {
  tabList: null,
};

export default withStyles(styles)(SimpleTabs);