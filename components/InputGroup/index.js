import React from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.text.checkbox + 36,
    borderRadius: '5px',

    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(2),
    }
  },
});

function InputGroup({ classes, children, title }) {
  return (
    <div className={classes.root}>
      <span>{title}</span>
      {children}
    </div>
  )
}

InputGroup.propTypes = {
  title: PropTypes.string,
};

InputGroup.defaultProps = {
  title: '',
};

export default withStyles(styles)(InputGroup);