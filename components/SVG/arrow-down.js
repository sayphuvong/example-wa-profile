import React from 'react'

export default function ArrowDown(props) {
  const {
    bgColor = 'white',
    width = '48px',
    height = '48px',
    isDepend,
  } = props;

  return (
    <svg
      width={isDepend ? '100%' : width}
      height={isDepend ? '100%' : height}
      viewBox="0 0 19 20"
      xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path d="M10.111 15.1883L18.7471 6.09741C19.0844 5.74244 19.0844 5.1668 18.747 4.81177C18.4097 4.45681 17.863 4.45681 17.5257 4.81184L9.50019 13.2599L1.4743 4.81177C1.13702 4.4568 0.590229 4.4568 0.252951 4.81183C0.0843105 4.98929 1.9043e-05 5.22196 1.90325e-05 5.45462C1.9022e-05 5.68729 0.0843105 5.91996 0.253008 6.09747L8.8896 15.1883C9.05156 15.3589 9.27121 15.4546 9.50025 15.4546C9.72928 15.4546 9.94899 15.3589 10.111 15.1883Z"
          fill={bgColor} />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect
            width="20"
            height="19"
            fill={bgColor}
            transform="translate(19) rotate(90)" />
        </clipPath>
      </defs>
    </svg>
  )
}
