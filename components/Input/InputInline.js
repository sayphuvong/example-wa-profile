import { withStyles } from '@material-ui/core';
import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';

import PadLockIcon from '../../components/SVG/padlock-icon';
import colors from '../../src/configs/colors';

const styles = (theme) => ({
  root: {
    display: 'flex',
    color: theme.palette.text.checkbox + '36',
    backgroundColor: theme.palette.text.checkbox + '24',
    marginBottom: '0.875rem',
    borderRadius: '5px',

    '& input': {
      width: '100%',
      color: theme.palette.text.checkbox,
      backgroundColor: 'transparent',
      paddingRight: '2.5rem',

      [theme.breakpoints.up('md')]: {
        paddingRight: '4.5rem',
      }
    }
  },
  gridLabel: {
    '& > label': {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      height: '100%',
      padding: '0px 0.75rem',
      color: theme.palette.text.checkbox,

      '&:after': {
        content: 'hhehe'
      },
    },
  },
  gridInput: {

  },
  divide: {
    color: theme.palette.text.checkbox,
    display: 'flex',
    alignItems: 'center',
  },
  inputWrap: {
    position: 'relative',
  },
  padlock: {
    width: '1.2rem',
    height: '1.2rem',
    position: 'absolute',
    top: '50%',
    right: '0.6rem',
    textDecoration: 'underline',
    transform: 'translateY(-50%)',
    color: theme.palette.text.checkbox,
    cursor: 'pointer',

    [theme.breakpoints.up('md')]: {
      right: '1rem',
    }
  },
  buttonStyle: {
    display: 'inline-block',
    textDecoration: 'underline',
    cursor: 'pointer',
    padding: '0.75rem',
    color: theme.palette.text.checkbox,
    fontSize: '1rem',
  }
});

function InputInline(props) {
  const {
    classes,
    value,
    onChange,
    label,
    as,
    valueOnClick,
  } = props;

  const handleInputChange = (e) => {
    if (onChange) {
      const value = e.target.value;
      onChange(value);
    }
  };

  const handleValueClick = (e) => {
    e.preventDefault();
    if (valueOnClick) {
      valueOnClick();
    }
  };

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={4} className={classes.gridLabel}>
          <label>{label}</label>
        </Grid>
        <Grid item xs={1} className={classes.divide}>:</Grid>
        <Grid item xs={7} className={classes.gridInput}>
          <div className={classes.inputWrap}>
            {as === 'input' && <input type="text" value={value} onChange={handleInputChange} />}
            {as === 'button' && <span className={classes.buttonStyle} onClick={handleValueClick}>{value}</span>}
            <div className={classes.padlock}>
              <PadLockIcon bgColor={colors.textColor} />
            </div>
          </div>
        </Grid>
      </Grid>

    </div>
  )
}

InputInline.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onVerify: PropTypes.func,
  type: PropTypes.string,
  as: PropTypes.string,
  valueOnClick: PropTypes.func,
};

InputInline.defaultProps = {
  classes: {},
  label: '',
  value: '',
  onChange: null,
  onVerify: null,
  type: 'text',
  as: 'input',
  valueOnClick: null,
};

export default withStyles(styles)(InputInline);