import { createMuiTheme } from '@material-ui/core/styles'
import colors from './configs/colors'

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1F2744',
    },
    secondary: {
      main: '#F94263',
    },
    error: {
      main: '#F44336',
    },
    background: {
      default: '#172038',
      secondary: '#1F2744',
      light: '#9FD8FF',
    },
    text: {
      default: '#FFFFFF',
      main: colors.textMainColor,
      secondary: '#6389AF',
      checkbox: '#9FD8FF',
      title: '#A1D6FA',
    },
  },
  typography: {
    fontFamily: [
      'Mulish',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 320,
      sml: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
})

theme.props = {
  MuiButton: {
    disableElevation: true,
  },
  MuiPaper: {
    elevation: 0,
  },
  MuiButtonBase: {
    disableRipple: true,
  }
}

theme.overrides = {
  MuiButton: {
    root: {
      textTransform: 'none',
    },
  },
  MuiTabs: {
    indicator: {
      backgroundColor: theme.palette.background.light,
    },
    scroller: {
      backgroundColor: theme.palette.background.secondary,
      color: theme.palette.text.main,
      borderBottom: '1px solid #000',
      borderBottomColor: theme.palette.text.secondary,
    },
  },
  MuiTab: {
    root: {
      textTransform: 'none',
    },
    wrapper: {
      flexDirection: 'row',
      justifyContent: 'center',
      '& > span': {
        width: '1.375rem',
        margin: '0px 0.5rem 0px 0px',
        display: 'flex',
        alignItems: 'center',
      }
    },
    labelIcon: {
      minHeight: 'unset',
    },
  },
}

export default theme