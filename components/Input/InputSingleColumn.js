import { withStyles } from '@material-ui/core';
import React from 'react'
import PropTypes from 'prop-types'

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    color: theme.palette.text.checkbox + '36',
    marginBottom: '0.875rem',

    '& input': {
      width: '100%',
      color: theme.palette.text.checkbox,
      backgroundColor: theme.palette.text.checkbox + '24',
      paddingRight: '4.5rem',
    }
  },
});

function MobileInput(props) {
  const {
    classes,
    value,
    onChange,
    label,
  } = props;

  const handleInputChange = (e) => {
    if (onChange) {
      const value = e.target.value;
      onChange(value);
    }
  };

  return (
    <div className={classes.root}>
      <label>{label}</label>
      <input type="text" value={value} onChange={handleInputChange} />
    </div>
  )
}

MobileInput.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onVerify: PropTypes.func,
  type: PropTypes.string,
};

MobileInput.defaultProps = {
  classes: {},
  label: '',
  value: '',
  onChange: null,
  onVerify: null,
  type: 'text',
};

export default withStyles(styles)(MobileInput);