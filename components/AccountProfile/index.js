import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import CardInfo from '../../components/CardInfo';
import Select from '../Input/Select';
import MobileInput from '../Input/MobileInput';
import Textarea from '../Input/Textarea';
import InputSingleCol from '../Input/InputSingleColumn';
import FromGroup from '../Form/FromGroup';
import InputInline from '../Input/InputInline';

const styles = (theme) => ({
  root: {
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(5.5),
    }
  },
  inputGroup: {
    display: 'flex',
    flexDirection: 'column',
    color: theme.palette.text.checkbox + '36',
    marginBottom: '0.875rem',
    
    '& input': {
      width: '100%',
      color: theme.palette.text.checkbox,
      backgroundColor: theme.palette.text.checkbox + '24',

      [theme.breakpoints.up('md')]: {
        paddingRight: '4.5rem',
      }
    }
  },
  commuWrap: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',

    [theme.breakpoints.up('md')]: {
      flexDirection: 'row',
    }
  },
  commuLeft: {
    flex: 1,
    padding: theme.spacing(2),
  },
  commuRight: {
    flex: 1,
    padding: theme.spacing(2),
  },
  paper: {
    backgroundColor: 'transparent',

    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(2),
    }
  },
  confirmCheckbox: {
    padding: theme.spacing(1),
    color: theme.palette.text.checkbox,
    fontWeight: 700,
    display: 'flex',
    justifyContent: 'center',
  },
  question: {
    padding: theme.spacing(1),
    color: theme.palette.text.title,
    textAlign: 'center',
  }
});
function AccountProfile({ personalData, classes }) {
  const [checked, setChecked] = useState(false);

  const handleCheckboxChange = (e) => {
    const _checked = e.target.checked;
    setChecked(_checked);
  };

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item md={6} sm={12} className={''}>
          <Paper className={classes.paper}>
            <CardInfo
              avatar={personalData.avatar}
              name={personalData.name}
              description={personalData.money.currency + ' ' + personalData.money.data}
            />

            <FromGroup title="Communication Details" theme="dark">
              <MobileInput
                label="Mobile Number"
                phone={personalData.phone}
                onChange={f => f}
                onVerify={f => f}
              />
              <Select label="Language" optionData={['Singapore']} />
            </FromGroup>
          </Paper>
          
        </Grid>

        <Grid item md={6} className={''}>
          <Paper className={classes.paper}>
            <FromGroup title="Account Details" theme="light">
              <InputInline
                label="Username"
                value={personalData.userName}
              />
              <InputInline
                label="First Name"
                value={personalData.name}
              />
              <InputInline
                label="Date of Birth"
                value={personalData.dateOfBirth}
              />
              <InputInline
                label="Password"
                value="Change Password"
                as="button"
                valueOnClick={f => f}
              />
              <InputInline
                label="Country"
                value={personalData.country}
              />
              <InputInline
                label="Currency"
                value={personalData.money.currency}
              />
              <InputInline
                label="Last Name"
                value={personalData.lastName}
              />
              <InputInline
                label="Email"
                value={personalData.email}
              />
            </FromGroup>
          </Paper>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12} className={''}>
          <Paper className={classes.paper}>
            <FromGroup title="Communication Details" theme="dark">
              <div className={classes.commuWrap}>
                <div className={classes.commuLeft}>
                  <Textarea
                    label="Address"
                    value={personalData.address}
                    onChange={f => f}
                    onVerify={f => f}
                  />
                </div>
                <div className={classes.commuRight}>
                  <InputSingleCol
                    label="Town/City"
                    value={personalData.townCity}
                    onChange={f => f}
                  />
                  <InputSingleCol
                    label="Postal Code"
                    value={personalData.postalCode}
                    onChange={f => f}
                  />
                </div>
              </div>
            </FromGroup>
          </Paper>
        </Grid>
      </Grid>

      <p className={classes.question}>Would you like to get the latest promos, updates, and offers?</p>
      <div
        className={classes.confirmCheckbox}
      >
        <FormControlLabel
          control={(
            <Checkbox checked={checked} onChange={handleCheckboxChange} name="confirm checkbox" />
          )}
          label="Yes. Send me the latest promotions, updates and offers."
        />
      </div>
    </div>
  )
}

AccountProfile.propTypes = {
  personalData: PropTypes.object,
};

AccountProfile.defaultProps = {
  personalData: null,
};

export default withStyles(styles)(AccountProfile);