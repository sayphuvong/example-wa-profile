import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

const styles = (theme) => ({
  root: {
    marginBottom: '1.5rem',
  },
  card: {
    padding: theme.spacing(3),
    backgroundColor: theme.palette.background.secondary,
    boxShadow: '0px 4px 34px rgba(0, 0, 0, 0.13);'
  },
  cardActionArea: {},
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    color: theme.palette.text.title,
  },
  userName: {
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '1.625rem',
  },
  userId: {
    fontWeight: 400,
    fontSize: '1rem',
  },
  media: {
    height: 140,
  },
  avatar: {
    width: '80px',
    height: '80px',
    margin: '0 auto',

    [theme.breakpoints.up('md')]: {
      width: '147px',
      height: '147px',
    }
  }
});

function CardInfo(props) {
  const {
    classes,
    avatar,
    name,
    description,
  } = props;
  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardActionArea className={classes.cardActionArea}>
          <Avatar alt="Remy Sharp" src={avatar} className={classes.avatar} />
          <CardContent className={classes.cardContent}>
            <Typography className={classes.userName} gutterBottom variant="h6" component="h2">
              {name}
            </Typography>
            <Typography className={classes.userId} gutterBottom variant="h6" component="h2">
              {description}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  )
}

CardInfo.propTypes = {
  name: PropTypes.string,
  description: PropTypes.string,
  avatar: PropTypes.string,
};

CardInfo.defaultProps = {
  name: '',
  description: '',
  avatar: '',
};

export default withStyles(styles)(CardInfo);