export default ({
  textColor: '#9FD8FF',
  textMainColor: '#C0E4FF',
  backgroundSecondary: '#1F2744',
});